## UI-framework setup:


## 1) Install Python. When the installation is finished, you can confirm the installation by opening up Command Prompt and typing the following command:
- python -V

Success! If all you need is Python 2.7 for some project or another, you can stop right here. It�s installed, the path variable is set, and you�re off to the races.

## 2) Pip install

## 3) Install Robot Framework to local machine (MAC/Windows):

The recommended installation method is using pip:
 - pip install robotframework-extendedselenium2library
The main benefit of using pip is that it automatically installs all dependencies needed by the library. Other nice features are easy upgrading and support for un-installation:
 - pip install --upgrade robotframework-extendedselenium2library

MAC:
      - sudo pip install robotframework   
      - sudo pip install robotframework-extendedselenium2library
Windows:
      - pip install robotframework
      - pip install robotframework-extendedselenium2library


## 4) Run tests: From Local environment:

    python start_robot.py -e DEV -s smoketests.test
-e
    DEV currently runs to DEV environment.
-s
    Smoke test set; you can configure any by adding .test files into 'sets' directory.

## 5) Test results can be found under 'reports' folder.