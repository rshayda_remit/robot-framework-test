__author__ = 'rshayda'

import argparse
import mmap
import os
import sys

PATH = os.path.dirname(os.path.realpath(__file__))

doc_path = ""
project_path = ""
results = [os.linesep, "The following files: ", os.linesep]
count = 0

include_folders = ["core"]

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description="""Generate robot framework documentation from given path.

Usage examples:
# Generate all
python generate_documentation.py
# Generate Core folder
python generate_documentation.py --core
# Generate file DesktopLogin.robot
python generate_documentation.py --file ../core/tests/login/login.robot

All generated files are html and put to Documentation directory.
Path argument is there to just be safe. It is very much recommended that this
script is used in a way that this argument is never needed.

""")
parser.add_argument('--path', default=PATH, help='Absolute path to Automation root directory')
parser.add_argument('--core', action="store_true", help="Create documentation of everything under core")
parser.add_argument('--file', default=False, help="Absolute path to file you want documented")


def start_program():
    args = parser.parse_args()

    if "documentation" in args.path:
        global doc_path
        global project_path
        doc_path = args.path
        project_path = os.path.split(doc_path)[0]
    else:
        project_path = args.path
        doc_path = os.path.join(args.path, "documentation")

    if not all(directories in os.listdir(project_path) for directories in include_folders):
        parser.error("Project path seems to be wrong as it doesn't contain core folder")

    if (len(sys.argv) == 3 and sys.argv[1] == "--path") or len(sys.argv) == 1:
        generate_all()

    if args.Core:
        path_to_core = os.path.join(project_path, "core")
        generate_docs(path_to_core)

    if args.file:
        file_path = os.path.abspath(args.file)
        if not os.path.split(args.file)[1].endswith((".robot", ".py")):
            parser.error("File extension is incorrect. File extension should be either '.robot' or '.py' ")

        if not os.path.isfile(args.file):
            parser.error(
                "No file named %s found in path %s" % (os.path.split(args.file)[1], os.path.split(args.file)[0]))

        else:
            generate_document(file_path)


def get_doc_type(file_name):
    tmp_var = open(file_name)
    robot_file = mmap.mmap(tmp_var.fileno(), 0, access=mmap.ACCESS_READ)
    if robot_file.find("*** Test Cases ***") != -1:
        return "testdoc"
    else:
        return "libdoc"


def get_doc_command(file_path, doc_type):
    file_name = os.path.split(file_path)[1].split(".")[0]
    file_name_with_type = "%s.html" % file_name
    cmd = "python -m robot.%s %s %s" % (doc_type, file_path, doc_path + os.sep + file_name_with_type)
    return cmd


def generate_document(file_path):
    if os.path.splitext(file_path)[1] == ".py":
        cmd = get_doc_command(file_path, "libdoc")
    elif os.path.splitext(file_path)[1] == ".robot":
        cmd = get_doc_command(file_path, get_doc_type(file_path))
    else:
        return

    if os.system(cmd) == 0:
        add_to_report(cmd)
    else:
        print "Error running command: %s" % cmd
        exit(3)


def generate_docs(path):
    for path, subdirs, files in os.walk(path):
        for name in files:
            generate_document(os.path.join(path, name))


def generate_all():
    for directory in include_folders:
        for path, subdirs, files in os.walk(os.path.join(project_path, directory)):
            for name in files:
                generate_document(os.path.join(path, name))


def add_to_report(command):
    results.append(os.path.split("".join(command.split(" ")[-1:]))[1])
    results.append(os.linesep)
    global count
    count += 1


def release_report():
    if count == 0:
        empty_results = [os.linesep, "Nothing to create! No files were found.", os.linesep]
        print "".join(empty_results)
    else:
        results.append(os.linesep)
        results.append("Were added to folder: ")
        results.append(doc_path)
        results.append(os.linesep)
        results.append("Total number of files created: %d" % (count))

        print "".join(results)


start_program()
release_report()
