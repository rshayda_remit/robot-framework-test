"""
Usage: python start_robot.py --environment/-e ENV --test-set/-s TESTSET
Purpose: This is the start script for DNA B2B GUI Test Automation


"""

import argparse
import os
import subprocess

# Variables
PATH = os.path.dirname(os.path.realpath(__file__))
reports_path = "reports"
tests_root = os.path.join("core", "tests")
sets_root = "sets"
errors = []
ok_environments = ["dev", "uat", "sit", "passion"]
report_filename = "output.xml"
xunit_reportfilename = "xunitreport.xml"

# Parse environment argument
parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=" Run DNA B2B Robot Framework GUI Tests")

parser.add_argument('-e', '--environment', help="Wanted environment (dev or uat or ...)", required=True)
parser.add_argument('-s', '--test-set', help="Testset to be executed. Default path is ROBOTPROJECT/sets", required=True)
args = parser.parse_args()

# Check that the environment argument matches ok_environments list
if args.environment.lower() in ok_environments:
    test_environment = "-v env:%s" % args.environment.lower()
else:
    print "[ERROR] No %s in list of Acceptable environments (%s)" % (args.environment.upper(), ok_environments)
    exit(1)

# Check that testset file is found'
if os.path.isfile(args.test_set):
    filename = args.test_set
elif os.path.isfile(os.path.join(PATH, sets_root, args.test_set)):
    filename = os.path.join(PATH, sets_root, args.test_set)
else:
    print "[ERROR] Testset %s not found in given path" % args.test_set
    exit(1)

tests = []
with open(filename) as tsfile:
    lines = tsfile.readlines()
    for line in lines:
        if line[0] == "#":
            continue
        else:
            tests.append(line.rstrip("\n").split(";"))

# Call all the test suites in this for loop
for index, test_params in enumerate(tests, 1):
    test_suite = os.path.join(tests_root, test_params[0])
    additional_variables = ""
    platform = ""
    browser_or_device = ""
    output_file = "output%d.xml" % index

    if len(test_params) == 2:
        additional_variables = test_params[1]

    pybot_call = " ".join(["pybot", "--outputdir", reports_path, "--output", output_file, test_environment,
                           additional_variables, test_suite])

    if subprocess.call(pybot_call, shell=True) != 0:
        error_content = [test_suite, platform, browser_or_device]
        errors.append(error_content)

# Remove redundant logs and reports as these are to be made again
os.remove(os.path.join(reports_path, "log.html"))
os.remove(os.path.join(reports_path, "report.html"))

# Merge reports
# Create rebot call by appending output-filenames to list
rebot_call = ["rebot", "--outputdir", reports_path, "-x", xunit_reportfilename, "--output", report_filename]
for i in range(1, len(tests) + 1):
    rebot_call.append(os.path.join(reports_path, "output%d.xml" % i))

rebot_call = " ".join(rebot_call)

# Make the rebot call
print "=" * 79
print "Merging outputs..."
if subprocess.call(rebot_call, shell=True) == (252 or 253 or 255 or 3):
    print "[ERROR] Something went wrong while trying to merge reports"
    exit(1)
print "Merge OK"

# Clean up extra reports
for i in range(1, len(tests) + 1):
    os.remove(os.path.join(reports_path, "output%d.xml" % i))

# Print pybot error summary, exit=1 if there are errors
print "=" * 79
if len(errors) != 0:
    print "Total number of failed test suites: %d" % len(errors)
    for error_run in errors:
        print os.linesep
        print "Error Trace: "
        print error_run
    print os.linesep
    exit(1)
