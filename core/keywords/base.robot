*** Settings ***
Documentation    description
Library     String
Library     Collections
Library     ../../core/libraries/chrome_emulator.py
#Resource    ../../Projects/${BRAND}/Environment/${BRAND}_${ENV}_properties.robot

*** Keywords ***
Open browser to homepage
    Run Keyword If  '${browser}' != 'firefox'
    ...     Set Selenium Implicit wait    30
    Run Keyword If  '${platform}' == 'mobile'
    ...     Open with emulator
    ...     ELSE IF    '${platform}' == 'desktop'
    ...     Run keywords
    ...     Open Browser    url=${homepage}     browser=${browser}    #remote_url=${HUB_URL}
    ...     AND    Maximize Browser Window


Open with emulator
    ${capabilities}=    Start webdriver with    ${device}
    Create webdriver    Chrome      desired_capabilities=${capabilities}    #command_executor=${HUB_URL}
    Go To      ${homepage}

Convert Platfrom and Browser to lowercase
    ${platform}=    Convert to Lowercase    ${platform}
    Set Global Variable    ${platform}    ${platform}
    ${browser}=    Convert to Lowercase    ${browser}
    Set Global Variable    ${browser}    ${browser}