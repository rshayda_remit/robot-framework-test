*** Settings ***
Documentation   This Library provides means to handle Log in and out related activities


*** Keywords ***
${username} logs in
    Login page is loaded
    Type ${USERNAME} and ${PASSWORD} and submit
    User is logged in

Given user is logged in
    Login page is loaded
    User logs in with valid credentials
    User is logged in

Login page is loaded
    [Documentation]
    Wait until element is visible    ${loginButton}


Type ${username} and ${password} and submit
    Input text      ${userNameField}    ${username}
    Input text      ${passwordField}    ${password}
    Click button    ${loginButton}

User logs in with valid credentials
    Type ${USERNAME} and ${PASSWORD} and submit

User is logged in
    Wait until element is visible    ${vacation_nav_panel_link}
    Element Should Not Be Visible    ${loginButton}







