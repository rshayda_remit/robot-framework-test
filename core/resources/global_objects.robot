*** Variables ***
${browser}     firefox
${device}      Google Nexus 5
${platform}    desktop
${env}         DEV


${invalidUsername}    invalid
${invalidPassword}    invalid
${testAutomationUser}    Testautomationuser
${emailSuffix}    example.lol

# If used of selenium Grid then uncomment base.robot
#${HUB_URL}     http://127.0.0.1:4444/wd/hub