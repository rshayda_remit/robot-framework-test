*** Settings ***
Library    ExtendedSelenium2Library
Library    OperatingSystem

Resource    ../../core/keywords/base.robot
Resource    ../../core/resources/global_objects.robot
Resource    ../../core/environment/${env}_properties.robot