*** Settings ***
Documentation    Test cases cover
...    Successful OAuth login followed by a logout,
...    Invalid user name,
...    Invalid password,
...    Invalid user name and password,
...    Empty user name,
...    Empty password,
...    Empty user name and password

Resource    ../../../core/resources/resources.robot
Resource    ../../../core/resources/global_objects.robot
Resource    ../../../core/objects/login_objects.robot
Resource    ../../../core/keywords/login_keywords.robot
Resource    ../../../core/environment/dev_properties.robot



Test Setup      Run Keywords    Open browser to homepage    AND    Set Selenium Timeout    60    AND    Set Selenium Speed    .7 seconds
Test Teardown   Close Browser

*** Test Cases ***
Successful login
    [Documentation]    OK: User is able to login with valid credentials
    [Tags]    Login
    Given login page is loaded
    When user logs in with valid credentials
    Then user is logged in
