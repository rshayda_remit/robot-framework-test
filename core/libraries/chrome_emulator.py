from selenium import webdriver

"""Custom webdriver starter library for Chrome Mobile Emulation

"""


def start_webdriver_with(device):
    """Returns a Chrome capabilities object which makes Chrome Emulation possible.

    Usage:
    | =Variable= | =Keyword= | =Argument= | =Comment= |
    | ${capabilities}= | Start webdriver with | iPhone 6 | # Sets iPhone 6 emulation settings to 'capabilities' |
    | ${capabilities}= | Start webdriver with | Nexus 5 | # Sets Nexus 5 emulation settings to 'capabilities' |
    """
    mobile_emulation = {"deviceName": device}
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)
    desired_capabilities = chrome_options.to_capabilities()
    return desired_capabilities
